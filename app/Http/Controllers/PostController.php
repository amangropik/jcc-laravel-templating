<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function index()
    {
        $post = DB::table('post')->get();
        return view('post.index', compact('post'));
    }


    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);
        $query = DB::table('post')->insert([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);
        return redirect('/post');
    }

    public function show($id)
    {
        $post = DB::table('post')->where('id', $id)->first();
        return view('post.show', compact('post'));
    }

    public function edit($id)
    {
        $post = DB::table('post')->where('id', $id)->first();
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);

        $query = DB::table('post')
            ->where('id', $id)
            ->update([
                'title' => $request["title"],
                'body' => $request["body"]
            ]);
        return redirect('/post');
    }

    public function destroy($id)
    {
        DB::table('post')->where('id',$id)->delete();
        return redirect('/post');
    }
}
